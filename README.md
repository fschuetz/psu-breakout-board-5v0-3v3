A psu design providing power from either a LiFePo4 battery or USB. Provides two power rails, one with 3.3V and one with 5V.

CAUTION: STATUS IS "IN DEVELOPMENT". There are still known shortcoming s of the
circuit and remodelling / testing is in progress. If you need a stable working
solution check back later.

Known open questions are:
- L2 has a too low current rating (800mA). It must support at least 2A.
- Under 800 mA load the 3V3 rail drops by about 0.27V. That seems a little much?

Note: All the calculations to customise the circuit can be found in 
[calculations.xlsx](calculations.xlsx)

## Features
Allows multiplexed power input from battery and USB-C and provides a 3.3V and
a 5.0V rail as well as a low power indicator (active low) set at approximately 
2.5V.

## Key Characteristics

### Input Sources
The psu provides two input sources, a battery and a USB connection. The USB
connection should provide a stable 5V, however, operation theoretically would
also work potentially as low as 3.2V input at the USB port due to the design
of the multiplexing circuit.

The battery should provide a voltage in the range of 2.5V up to 4.5V. This
makes the PSU well suited to use with either two or three cell alkaline, NiCd
or NiMH or single-cell Li batteries (LiPo, LiFePo).

### Maximal Output Current
The maximum output current in over both power rails is limited by the max. 
rating of the input source or the sum of the max. output current of the 3V3 
and the 5V rail combined, whatever is lower.

```
I_out_max = min(I_out_max_src, I_out_max_5V0 + I_out_max_3V3)

where:
- I_out_max_3V3 = 800mA
- I_out_max_5V0 = 1A
```

The 3V3 power rail can provide a maximum of 800mA. It has to be noted, that the
max current can supply as much as 1200mA as long as the source stays above the 
3.3V (due to the fact that the regulator can supply more current in buck mode 
than in boost mode).

The 5V0 rail is designed to provide a maximal current of 1A. The design can
be adjusted to support up to 4A (4.5 A peak) on the 5V0 rail. 

## Bill of Material (BOM)
The bill of material can directly be generated from KiCAD. In order to ensure
changes are reflected properly make sure to update component properties 
accordingly.

TODO

Note that the design is such that all parts can be sourced easily. Thus please
always provide an order number in the form of #vendor_name as a field name
and the order number as the value if sending pull requests for changes. Also 
make sure to add the datasheet for this part. (Of course parts can be sourced
at different vendors. This is for convenience only).

## Design Choices

### Power Multiplexing
TODO

### Battery Protection
Currently no battery protection is build into the circuit. Only a low battery
indicator is provided ath the output. The indicator is set to trigger at 2.5V.

### 3V3 Conversion
The key component to provide a 3V3 power rail is the [TPS63000 Buck-Boost Converter](https://www.ti.com/lit/ds/symlink/tps63000.pdf)
from Texas Instruments. The TPS6300 uses a reference voltage steered by a 
voltage divider to set the output voltage. Theoretically the TPS63001 that
provides a fixed 3.3V output could be used. The choice for the TPS63000 was
made as this chip is significantly cheaper than the TPS6301. If you use the
TPS6301 just leave R4 and R5 unpopulated.

#### Output Voltage Setting
The output Voltage is set using a voltage divider over pin 10 (FB). The two
resistors R4 and R5 form this divider. The following values were chosen:

```
R4 = 1MΩ, R5 = 178kΩ
```

R5 should be around 200kΩ. Using a 1MΩ resistor for R4 requires a 178kΩ 
resistor as R5, which is close enough at 200kΩ. Both resistor values are
easily available.

For more stable feeback we add a feedforward capacitor C9 of 2.2pF. The value
is calculated as follows:

```
C_ff = 2.2us / R4 = 2.2us / 1MΩ = 2.2p
```

#### Choice of Inductance

### 5V0 Conversion
The key component to provide a 5V0 power rail is the [TPS61032 Boost Converter](https://www.ti.com/lit/ds/symlink/tps61030.pdf)
from Texas Instruments. The TPS61032 is a fixed voltage boost converter. While
there exists a version with adjustable output voltage (TPS61030) that is 
cheaper than the TPS61032, at the time of design the TPS61032 was chosen due to
better availability and easier sourcing.

#### LBO Threshold Voltage
The current through the divider should be about 100 times greater than the
current into the LBI pin. The typical current into the LBI pin is 0.01uA and
the voltage across R8 is equal to the LBI voltage threshold generated on chip,
which has a value of 500mV. Threrfore R8 should be in the rangeof 500kΩ. 
The formula for calculating R7 and R8 is:

```
R7 = R8 * (V_bat / (V_LBI_threshold)-1)
```

Choosing 390kΩ for R7 and 1.54MΩ for R8 sets the battery threshold voltage at
2.47V, which is just a little below 2.5V. We chose 1.54MΩ as this resistor 
value can also be used as the R9 pull up for the LBO. 

#### Selection of the Inducatance
Two parameters decide how to choose a boost inductor: The necessary average
current flow the inductor must support and the desired current ripple in the
inductor.
It is recommended to keep the possible peak inductor current below the current 
limit threshold of the power switch. We choose the current limit for the 5V 
rail at the maximum 4.5A. The peak current can be calculated as 
```
For 2A:
I_L = I_out * (V_out / (V_bat * 0.8)) = 2A * (5V / (2.5V * 0.8)) = 5A

For maximum output the calculation would be:
I_L = I_out * (V_out / (V_bat * 0.8)) = 4.5A * (5V / (2.5V * 0.8)) = 11.25A
```
where I_L is largest when the battery voltage is lowest, in our case 2.5V. 
The calculation shows our inductance must allow at least 11.25A of average
current flow.

Normally it is advisable to work with a ripple of less than 20% of the average
inductor current. Smaller ripple reduces the magneetic hysteresis losses in
the inductor, as well as output voltage ripple and EMI. But in the same way, 
regulation time at load changes rises and a larger inductor increases cost.
The inductor can be calculated as follows:
```
L2 = ((V_bat * (V_out - V_bat)) / (▲I_L * f * V_out))
  = ((2_5V * (5 - 2.5V)) / (11.25A * 0.1 * 600kHz * 5)) = 4.17uH

However, we choose L2 = 6.8 uH
```
We chose 10% for the ripple current and again 2.5V as this will lead to 
the highest inductance. The result means we could go as low as 1.85uH
for the inductance. However, the datasheet recommends 6.8uH for typical
applications, so we will go with this.
